<?php

namespace addons\shopro\controller;


class ShopCard extends Base
{

    protected $noNeedLogin = [];
    protected $noNeedRight = ['*'];


    public function charge()
    {
        $params = $this->request->post();
        
        // 表单验证
        $this->shoproValidate($params, get_class(), 'charge');

        $card = \addons\shopro\model\ShopCard::charge($params);

        $this->success('充值成功');
    }
}
