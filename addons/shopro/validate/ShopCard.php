<?php

namespace addons\shopro\validate;

use think\Validate;

class ShopCard extends Validate
{

    /**
     * 验证规则
     */
    protected $rule = [
        'card_no' => 'require|number',
        'card_password' => 'require'
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'card_no.require' => '卡号必须',
        'card_no.number' => '卡号格式错误',
        'card_password.require' => '卡密必须',
    ];

    /**
     * 字段描述
     */
    protected $field = [
        
    ];

    /**
     * 验证场景
     */
    protected $scene = [
        'charge' => ['card_no', 'card_password'],
    ];

}
