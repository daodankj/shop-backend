<?php

namespace addons\shopro\model;

use think\Model;
use addons\shopro\model\User;
use addons\shopro\exception\Exception;
use think\Db;

/**
 * 充值卡
 */
class ShopCard extends Model
{
    // 表名,不含前缀
    protected $name = 'shop_card';
    // 自动写入时间戳字段
    /*protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';*/


    // 追加属性
    protected $append = [];

    // 充值
    public static function charge($params)
    {
        
        $user = User::info();
        extract($params);
        
        $shop_card = self::get(['card_no'=>$card_no,'card_password'=>strtoupper($card_password)]);
        if (!$shop_card || $shop_card->status==0) {
            new Exception('该卡不存在');
        }

        if ($shop_card->charge_time>0) {
            new Exception('该卡已被使用过');
        }

        $shop_card = Db::transaction(function() use ($shop_card,$user){

            $shop_card->charge_time = time();//标记使用
            $shop_card->user_id = $user->id;
            $shop_card->save();
            //加余额
            User::money($shop_card->money, $user->id, 'shop_card', $shop_card->id, '充值卡充值', [
                'shop_card_id' => $shop_card->id,
                'card_no' => $shop_card->card_no
            ]);

            return $shop_card;
        });

        return $shop_card;
    }
}
