<?php

namespace addons\yktsms\library;

use fast\Http;

/**
 * 短信
 */
class YktMsg
{
    //const SEND_URL = "http://sms.189ek.com/yktsms/send";
    private $url;
    private $appid;
    private $appkey;
    /**
     * 构造函数
     *
     * @param string $appid  sdkappid
     * @param string $appkey sdkappid对应的appkey
     */
    public function __construct($appid, $appkey)
    {
        $this->url = "http://sms.189ek.com/yktsms/send";
        $this->appid =  $appid;
        $this->appkey = $appkey;
    }

    /**
     * 单例
     * @param array $options 参数
     * @return 
     */
    public static function instance($appid, $appkey)
    {
        if (is_null(self::$instance))
        {
            self::$instance = new static($appid, $appkey);
        }
        return self::$instance;
    }

    /**
     * 发送短信
     * @param string mobile
     * @return array
     */
    public function send($mobile = '',$msg='' ,$sign='' ,$templateID='')
    {
        $msg =$sign.$msg;
        $umsg = urlencode($msg);
        $msgsign = md5($this->appid.$mobile.$msg.$this->appkey);
        $queryarr = array(
            "appid"      => $this->appid,
            "mobile"     => $mobile,
            "msg"        => $umsg,
            "sign"       => $msgsign
        );
        $url = $this->url."?appid=".$queryarr['appid']."&mobile=".$queryarr['mobile']."&msg=".$queryarr['msg']."&sign=".$queryarr['sign'];
        $response = Http::get($url);
        $result = explode(',', $response);
        //$ret = (array)json_decode($response, true);
        return $result ? $result : [];
    }
}
