<?php

namespace addons\yktsms;

use app\common\library\Menu;
use think\Addons;

/**
 * 插件
 */
class Yktsms extends Addons
{

    private $appid = null;
    private $appkey = null;
    private $config = null;
    private $sender = null;

    public function ConfigInit()
    {
        $this->config = $this->getConfig();
        $this->appid = $this->config['appid'];
        $this->appkey = $this->config['appkey'];
    }

    /**
     * 短信发送行为
     * @param   Sms $params
     * @return  boolean
     */
    public function smsSend(&$params)
    {
        $this->ConfigInit();
        if (isset($params->sign)&&$params->sign) {
            $sign = $params->sign;
        }else{
            $sign = $this->config['sign'];
        }
        try {
            if ($this->config['isTemplateSender'] == 1) {
                $templateID = $this->config['template'][$params->event];
                //普通短信发送
                $this->sender = new library\YktMsg($this->appid, $this->appkey);
                //$this->sender = library\YktMsg::instance($this->appid, $this->appkey);
                $result = $this->sender->send($params['mobile'], $params['code'],$sign,$templateID);
            } else {
                $this->sender = new library\YktMsg($this->appid, $this->appkey);
                $result = $this->sender->send($params['mobile'], $params['code'],$sign);
            }

            if ($result[0] == 0) {
                return true;
            } else {
                return false;
            }
        } catch (\think\exception\ErrorException $e) {
            //\think\Log::error('短信发送失败，错误信息：' . $e->getMessage());
            //var_dump($e);
            //exit();
        }
    }

    /**
     * 短信发送通知
     * @param   array $params
     * @return  boolean
     */
    public function smsNotice(&$params)
    {
        $this->ConfigInit();
        if (isset($params->sign)&&$params->sign) {
            $sign = $params->sign;
        }else{
            $sign = $this->config['sign'];
        }
        try {
            if ($this->config['isTemplateSender'] == 1) {
                $templateID = $this->config['template'][$params->event];
                //普通短信发送
                //$this->sender = library\YktMsg::instance($this->appid, $this->appkey);
                $this->sender = new library\YktMsg($this->appid, $this->appkey);
                $result = $this->sender->send($params['mobile'], $params['msg'],$sign,$templateID);
            } else {
                //$this->sender = library\YktMsg::instance($this->appid, $this->appkey);
                $this->sender = new library\YktMsg($this->appid, $this->appkey);
                $result = $this->sender->send($params['mobile'], $params['msg'],$sign);
            }

            if ($result[0] == 0) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            //\think\Log::error('短信发送失败，错误信息：' . $e->getMessage());
            var_dump($e->getMessage());
            exit();
        }
    }

    /**
     * 检测验证是否正确
     * @param   Sms $params
     * @return  boolean
     */
    public function smsCheck(&$params)
    {
        return TRUE;
    }

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        
        return true;
    }

    /**
     * 插件启用方法
     * @return bool
     */
    public function enable()
    {
        
        return true;
    }

    /**
     * 插件禁用方法
     * @return bool
     */
    public function disable()
    {
        
        return true;
    }

}
