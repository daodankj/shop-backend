<?php

namespace tool;

use fast\Http;

/**
 * 短信
 */
class YktMsg
{
    const SEND_URL = "http://sms.189ek.com/yktsms/send";

    /**
     * 配置信息
     * @var array
     */
    private $config = [
        'app_id' => 'ldz3AXTG981JsQcriIpwznJCsEMwYulU',
        'app_secret' => 'hHCfMIcqW6lVuuSTJi2DJeXyvJGO6EXo'
    ];

    public function __construct($options = [])
    {
        $this->config = array_merge($this->config, is_array($options) ? $options : []);
    }

    /**
     * 发送短信
     * @param string mobile
     * @return array
     */
    public function sendMsg($mobile = '',$msg='')
    {
        if (!$mobile||!$msg) {
            return [];
        }
        $msg ='【肇庆高新区】'.$msg;
        $umsg = urlencode($msg);
        $sign = md5($this->config['app_id'].$mobile.$msg.$this->config['app_secret']);
        $queryarr = array(
            "appid"      => $this->config['app_id'],
            "mobile"     => $mobile,
            "msg"        => $umsg,
            "sign"       => $sign
        );
        $url = self::SEND_URL."?appid=".$queryarr['appid']."&mobile=".$queryarr['mobile']."&msg=".$queryarr['msg']."&sign=".$queryarr['sign'];
        $response = Http::get($url);
        $result = explode(',', $response);
        //$ret = (array)json_decode($response, true);
        return $result ? $result : [];
    }
}
