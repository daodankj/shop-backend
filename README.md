

## 安装使用
1、克隆项目到你本地
2、进入目录
3、下载前端插件依赖包
   bower install
4、下载PHP依赖包
   composer install

参考文档 https://doc.fastadmin.net/doc/install.html
