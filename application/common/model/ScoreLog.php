<?php

namespace app\common\model;

use think\Model;

/**
 * 会员积分日志模型
 */
class ScoreLog Extends Model
{

    // 表名
    protected $name = 'user_score_log';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = '';
    // 追加属性
    protected $append = [
        'user_name',
        'mobile',
        'idcard',
        'child_name',
        'school_name'
    ];

    protected function getUserNameAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');

        return $name?$name:'';
    }

    protected function getMobileAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('mobile');

        return $name?$name:'';
    }

    protected function getIdcardAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('idcard');

        return $name?$name:'';
    }

    protected function getChildNameAttr($value,$data){
        $name = db('active_baoming')->where(['user_id'=>$data['user_id']])->value('uname');

        return $name?$name:'';
    }

    protected function getSchoolNameAttr($value,$data){
        $name = db('active_baoming')->where(['user_id'=>$data['user_id']])->value('library_name');

        return $name?$name:'';
    }
}
