<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use app\common\library\Sms;
use fast\Random;
use think\Validate;
use addons\shopro\library\Wechat;
use addons\shopro\model\UserOauth;
use addons\shopro\model\User as UserModel;
use addons\shopro\model\UserStore;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        //echo dechex(20210909203541); 
        $a = strtoupper(substr(md5(2),8,16));
        //echo join("-",str_split($a,4));
        return $this->view->fetch();
    }


    /**
     * 大旺小程序自动登入绑定注册一条龙服务
     *
     * @param string $code 加密code
     */
    public function auto_login()
    {
        $idCardNumber = $this->request->get('idCardNumber');
        $uname = $this->request->get('uname');
        $mobile = $this->request->get('tel');
        if (!$idCardNumber||!$uname||!$mobile) {//未获取到认证信息提示错误
            $this->redirect('https://ghfl.zqci.cn/');
        }
        $token = session('user_token');
        if ($token) {
        	header('Location: https://ghfl.zqci.cn/pages/public/loading/?token=' . $token);
        	exit;
        }
    	$back_url = url('index/index/auto_info',['name'=>$uname,'idcard'=>$idCardNumber,'mobile'=>$mobile],'',true);
        $wechat = new Wechat('wxOfficialAccount');
        $oauth = $wechat->oauth();
        $response = $oauth->scopes(['snsapi_userinfo'])->redirect($back_url);
        $response->send();
    }
    public function auto_info($name='',$idcard='',$mobile=''){
    	if (!$idcard||!$name||!$mobile) {//未获取到认证信息提示错误
            $this->redirect('https://ghfl.zqci.cn/');
            return;
        }
        $getUser = true;//标记是否获取到用户信息，兼容拒绝获取用户信息的操作

    	try{
    		$wechat = new Wechat('wxOfficialAccount');
	        $oauth = $wechat->oauth();
	        $decryptData = $oauth->user()->getOriginal();
    	}catch (\Exception $e) {	//获取用户信息异常   \Overtrue\Socialite\AuthorizeFailedException
            //$this->error($e->getMessage());
            //return $e->getMessage();
            $getUser = false;
            $decryptData['nickname'] = $name;
            $decryptData['headimgurl'] = '';
        }
        $platform = 'wxOfficialAccount';
    	$provider = 'Wechat';
    	$oauthData = $decryptData;
        $oauthData = array_merge($oauthData, [
            'provider' => $provider,
            'platform' => $platform,

        ]);
        
        //在判断手机是否注册,先不根据身份证判断
        $uid = db('user')->where(['mobile'=>$mobile])->value('id');
        if(!$uid){//手机号码也没注册，则自动注册并且绑定微信
    		$username = $name;
            $password = '';
            $extend = $this->getUserDefaultFields();
            $extend['nickname'] = $oauthData['nickname'];
            $extend['avatar'] = $oauthData['headimgurl'];
            $extend['idcard'] = $idcard;
            $this->auth->register($username, $password, $mobile . '@qq.com', $mobile, $extend, 0);
    	}else{
    		$this->auth->direct($uid);
    	}
    	if ($getUser) {
    		//判断微信是否注册
	        $userOauth = UserOauth::where('openid', $decryptData['openid'])->where('platform', $platform)->where('provider', $provider)->lock(true)->find();

	        if (!$userOauth) {      // 没有找到第三方登录信息 创建新用户
	        	//默认创建新用户
	            $oauthData['logintime'] = time();
	            $oauthData['createtime'] = time();
	            $oauthData['logincount'] = 1;
	            $oauthData['user_id'] = $this->auth->getUser()->id;
	            UserOauth::strict(false)->insert($oauthData);
	        }else {
	            // 找到第三方登录信息，直接登录
	            $user_id = $this->auth->getUser()->id;
	            if($this->auth->direct($user_id)) {       // 获取到用户
	                $oauthData['logincount'] = $userOauth->logincount + 1;
	                $oauthData['logintime'] = time();
	                $oauthData['user_id'] = $this->auth->getUser()->id;//重新绑定
	                $userOauth->allowField(true)->save($oauthData); 
	            }
	        }
    	}
    	
        //登入跳转
        $token = '';
        if($this->auth->getUser()) {
            $this->setUserVerification($this->auth->getUser(), $provider, $platform);
            $token = $this->auth->getToken();
        }
        // 跳转回前端
        if ($token) {
        	session('user_token',$token);
            header('Location: https://ghfl.zqci.cn/pages/public/loading/?token=' . $token);
        } else {
        	echo $this->auth->getError();
            //header('Location: https://ghfl.zqci.cn');
        }
        exit;
    }

    private function getUserDefaultFields()
    {
        $userConfig = json_decode(\addons\shopro\model\Config::get(['name' => 'user'])->value, true);
        return $userConfig;
    }
    private function setUserVerification($user, $provider, $platform)
    {
        $verification = $user->verification;
        $verification->mobile = 1;
        if ($platform === 'App') {
            $platform = '';
            if ($provider === 'Wechat') {
                $platform = 'wxOpenPlatform';
            } elseif ($provider === 'Alipay') {
                $platform = 'aliOpenPlatform';
            }
        }
        if ($platform !== '') {
            $verification->$platform = 1;
            $user->verification = $verification;
            $user->save();
        }
    }

}
