<?php
namespace app\index\controller;
use EasyWeChat\Factory;
use think\Session;
use app\common\controller\Frontend;

class Test extends Frontend
{
	protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';
    protected $wechat = null;
	// 初始化
	public function _initialize()
	{
		//$config = get_addon_config('wechat'); //此处 看文档末尾
		$config = [
		    'app_id' => 'wx378f7e0b3d35285b',
		    'secret' => '8e3f8f3e27b8edf50393250720aaeb5a',
		    // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
		    'response_type' => 'array',
		];
		$config['oauth']['scopes'] = ['snsapi_userinfo'];
		$config['oauth']['callback'] = "https://ydmls.zqgx.gov.cn/index.php/index/test/home"; //回调地址
		$this->wechat = Factory::officialAccount($config); 
	}
	public function index()
    {
    	/*if (empty(Session::get("wechat_user"))) {
        	$response = $this->wechat->oauth->scopes(['snsapi_userinfo'])->redirect();
        	$response->send();
        }*/
        // $token = Session::get("wechat_user")['token']->access_token;
        // 用户收货地址
        $js = $this->wechat->jssdk->buildConfig(['checkJsApi','openAddress'],$debug = true, $beta = false, $json = true); //openAddress是地址列表

        $this->assign("js",$js); // 到页面
        return $this->view->fetch();
    }
    public function home()
    {
    	$user = $this->wechat->oauth->user();
    	$userInfo = User::get(['open'=>$user->id]); // 数据库根据openid查找数据

    	if (!$userInfo) {
    		// 进行注册
    		//此处代码略过
    	}
    	// 进行登录
    	Session::set("wechat_user",$user->toArray()); // 将openid等信息转化为数组
    	Session::set("user_info",$userInfo->toArray()); //将数据表查询的信息转化为数组
    	$this->success("登录成功","index"); 
    }

    public function testjob($id=0){
        /*if (!extension_loaded('redis')) {
            throw new \BadFunctionCallException('not support: redis');
        }
        return 'OK';*/
        /*$job = db('jobs')->where('id='.$id)->find();
        if (!$job) {
            return -1;
        }
        $info = json_decode($job['payload'],true);
        $data = $info['data'];
        $notifiables = $data['notifiables'];
        $notification = $data['notification'];
        // 因为 notification 只有参数，需要把对应的类传过来，在这里重新初始化
        $notification_name = $data['notification_name'];

        // 重新实例化 notification 实例
        if (class_exists($notification_name)) {
            $notification = new $notification_name($notification['data']);

            // 发送消息
            \addons\shopro\library\notify\Notify::sendNow($notifiables, $notification);
        }*/

        try {
            $num = 0;
            $num ++;
            $obj = $this->getredis();
            $num++;
            echo  $num;
        } catch (\Exception $e) {
            // 队列执行失败
            //echo $e->getMessage();
        }
    }
    public function  getredis(){
        if (!extension_loaded('redis')) {
            throw new \BadFunctionCallException('not support: redis');
        }
        return new \redis();
    }
}