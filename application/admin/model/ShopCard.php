<?php

namespace app\admin\model;

use think\Model;


class ShopCard extends Model
{

    

    

    // 表名
    protected $name = 'shop_card';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'charge_time_text'
    ];
    

    protected function getCardPasswordAttr($value, $data){
        return join("-",str_split($value,4));
    }

    protected function getChargeTimeTextAttr($value, $data){

        return $data['charge_time']==0?'未充值':date('Y-m-d H:i:s');
    }





}
