<?php

return [
    'autoload' => false,
    'hooks' => [
        'app_init' => [
            'epay',
            'log',
            'qrcode',
            'shopro',
        ],
        'admin_login_init' => [
            'loginbg',
        ],
        'config_init' => [
            'nkeditor',
            'yktsms',
        ],
        'upgrade' => [
            'shopro',
        ],
        'sms_send' => [
            'yktsms',
        ],
        'sms_notice' => [
            'yktsms',
        ],
        'sms_check' => [
            'yktsms',
        ],
    ],
    'route' => [
        '/qrcode$' => 'qrcode/index/index',
        '/qrcode/build$' => 'qrcode/index/build',
    ],
    'priority' => [],
];
