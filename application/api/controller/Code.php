<?php

namespace app\api\controller;

use app\common\controller\Api;

/**
 * 微信网页授权中转
 */
class Code extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    //发起授权地址
    public function index($ask_type='',$url='')
    {
        if($ask_type){
            if($ask_type == 'ydmls'){
                $action = "ydmls";
                cache('wxbackurl_ydmls',urldecode($url));
            }else{
                $action = "index";
            }
            //发起授权
            $appid = "wx378f7e0b3d35285b";  
            $redirect_url = url($action,'','',true);
            $code_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".urlencode($redirect_url)."&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
            header("location: ".$code_url);    
        }else{
            echo "请求参数错误";
        }
    }

    /**
     * 阅读马拉松
     *
     */
    public function ydmls($code='',$state='')
    {
        if($code){
            $ydmls = cache('wxbackurl_ydmls');
            header("location:".$ydmls."?code=".$code."&state=".$state);
        }else{
            echo '请求参数错误';
        } 
    }


}
