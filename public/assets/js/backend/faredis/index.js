define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template', 'jstree'],
    function ($, undefined, Backend, Table, Form, Template, jstree) {
        var SELECTED_NODE = null;
        var KEYDATA = null;
        var Controller = {
            jstreeoptions: function () {
                var options = {
                    "types": {
                        "root": { "icon": "/assets/addons/faredis/img/redis_16.png" },
                        "db": { "icon": "/assets/addons/faredis/img/db_16.png" },
                        "loading": { "icon": "/assets/addons/faredis/img/loading.gif" },
                        "key": { "icon": "/assets/addons/faredis/img/key_16.png" }
                    },
                    "plugins": ["types", "wholerow", "contextmenu"],
                    "contextmenu": {
                        "items": Controller.menu
                    },
                    "state": {
                        'opened': true
                    },
                    'core': {
                        "data": {
                            "url": "faredis/index",
                        },
                        "check_callback": true,
                    }
                };
                return options;
            },
            jstreeevent: function () {
                $('#tree').bind("select_node.jstree", function (event, data) {
                    var inst = data.instance;
                    var selectedNode = inst.get_node(data.selected);
                    SELECTED_NODE = selectedNode;
                    var level = $("[id^='"+selectedNode.id+"']").attr('aria-level');
                    if (level == 2) {
                        if (selectedNode.children.length > 0) {
                            return;
                        }
                        inst.set_type(selectedNode, 'loading');
                        Controller.loadChild(inst, selectedNode);
                    }
                    else if (level == 3) {
                        var tmp = selectedNode.id.split('_');
                        var db = tmp[0];
                        tmp.shift();
                        var key = tmp.join('_');
                        Controller.loadValue(db, key);
                    }
                });
            },
            isJSON:function(str){
                if(typeof str == 'string'){
                    try{
                        var obj = JSON.parse(str);
                        if(typeof obj =='object' && obj){
                            return true;
                        }
                        else return false;
                    }
                    catch(e){
                        return false;
                    }
                }
            },
            jsonFormat:function(el){
                if(Controller.isJSON($(el).val())){
                    $(el).val(JSON.stringify(eval('('+$(el).val()+')'),null,4));
                }
            },
            loadChild: function (inst, selectedNode) {
                $.post("faredis/index/keys", { db: selectedNode.id - 1 }, function (res) {
                    selectedNode.children = [];
                    $.each(res, function (i, item) {
                        inst.create_node(selectedNode, item, 'last');
                    });
                    inst.open_node(selectedNode);
                    inst.set_type(selectedNode, 'db');
                });
            },
            loadValue: function (db, key) {
                $.post('faredis/index/getValue', { db: db, key: key }, function (res) {
                    //reset
                    $('#c-value').val('');
                    $('.size').text('');
                    $('.set-value').val('');
                    $('.hash-key').val('');
                    $('.hash-value').val('');
                    $('.zset-value').val('');
                    $('.zset-score').val('');
                    KEYDATA = res;
                    $('.p-title').text("db" + db + "::" + key);
                    $('.c-key').val(key);
                    $('.c-db').val(db);
                    $(".ttl").text(res.ttl);
                    switch (res.type) {
                        case 1://string
                            $('.string').show();
                            $('.set').hide();
                            $('.hash').hide();
                            $('.zset').hide();
                            $('#c-value').val(res.value);
                            Controller.jsonFormat( $('#c-value'));
                            break;
                        case 2://set
                        case 3://list
                            $('.type-lb').text(res.type == 2 ? "SET:" : "LIST:");
                            $('.set').show();
                            $('.string').hide();
                            $('.hash').hide();
                            $('.zset').hide();
                            // $('#set-value').val(res.value);
                            var html = '';
                            if (res.type == 2) {
                                res.value.forEach(v => {
                                    html += "<option value='" + v + "'>" + v + "</option>"
                                });
                            }
                            else {
                                var lidx = 0;
                                res.value.forEach(v => {
                                    html += "<option value='" + lidx++ + "'>" + v + "</option>"
                                });
                            }
                            $('#set-list').html(html);
                            $('.size').text(res.value.length);
                            break;
                        case 4:
                            $('.type-lb').text("ZSET:");
                            $('.hash').hide();
                            $('.string').hide();
                            $('.set').hide();
                            $('.zset').show();
                            let count4 = 0;
                            Object.keys(res.value).forEach(v => {
                                html += "<option value='" + v + "'>" + v + "</option>"
                                count4++;
                            });
                            $('#zset-list').html(html);
                            $('.size').text(count4);
                            break;
                        case 5://hash
                            $('.type-lb').text("HASH:");
                            $('.hash').show();
                            $('.string').hide();
                            $('.set').hide();
                            $('.zset').hide();
                            var html = '';
                            let count = 0;
                            Object.keys(res.value).forEach(v => {
                                html += "<option value='" + v + "'>" + v + "</option>"
                                count++;
                            });
                            $('#hash-list').html(html);
                            $('.size').text(count);
                            break;
                        default:
                            $('.string').hide();
                            $('.set').hide();
                            break;
                    }
                });
            },
            menu: function (node) {
                var items = {
                    "reload": {
                        "label": "重载",
                        "icon": "fa fa-refresh",
                        "action": function (data) {
                            console.log('reload');
                            var inst = $.jstree.reference(data.reference);
                            obj = inst.get_node(data.reference);
                            $.post('faredis/index/reloadDb?db=' + obj.id, null, function (res) {
                                console.log(res);
                                $("#tree").jstree('set_text', obj, res.text);
                                Controller.loadChild(inst, obj);
                            })
                        }
                    },
                    "flush": {
                        "label": "清空",
                        "icon": "fa fa-trash",
                        "action": function (data) {
                            console.log('flush');
                            var inst = $.jstree.reference(data.reference);
                            obj = inst.get_node(data.reference);
                            console.log(obj);
                            Layer.confirm("确认清空吗？", function () {
                                $.post("faredis/index/flushDb", { db: obj.id }, function (res) {
                                    Layer.closeAll('dialog');
                                    $("#tree").jstree('set_text', SELECTED_NODE, "DB" + (obj.id - 1) + "(0)");
                                    var childNodes = inst.get_children_dom(obj);
                                    $.each(childNodes, function (index, item) {
                                        inst.delete_node(item);
                                    })
                                });
                            });
                        }
                    },

                };
                if (node.type == 'db') {
                    return items;
                }
            },
            index: function () {

                $('#tree').jstree(Controller.jstreeoptions());
                Controller.jstreeevent();
                $('.rds_del').on('click', function () {
                    Layer.confirm("确认删除？", function () {
                        $.post("faredis/index/delKey", { db: KEYDATA.db, key: KEYDATA.key }, function (res) {
                            Layer.closeAll('dialog');
                            $("#tree").jstree('set_text', SELECTED_NODE, KEYDATA.key + "(Removed)");
                        });
                    });

                });
                $('.rds_ttl').on('click', function () {
                    Fast.api.open("faredis/index/ttl?db=" + KEYDATA.db + "&key=" + KEYDATA.key, "修改TTL", {
                        area: "[200px,50px]"
                    });
                });
                $('.rds_rename').on('click', function () {
                    Fast.api.open("faredis/index/rename?db=" + KEYDATA.db + "&key=" + KEYDATA.key, "修改名称", {
                        area: "[200px,50px]"
                    });
                });
                //set list 
                $('#set-list').on('click', function () {
                    if ($('.type-lb').first().text() == 'LIST:') {
                        $('.set-value').val($(this).find("option:selected").text());
                    } else
                        $('.set-value').val($(this).val());
                    Controller.jsonFormat( $('.set-value'));
                });
                //hash
                $('#hash-list').on('click', function () {
                    let key = $(this).val();
                    $('.hash-key').val(key);
                    $('.hash-value').val(KEYDATA.value[key]);
                    Controller.jsonFormat( $('.hash-value'));
                });
                //zset
                $('#zset-list').on('click', function () {
                    let key = $(this).val();
                    $('.zset-value').val(key);
                    $('.zset-score').val(KEYDATA.value[key]);
                });

                Form.api.bindevent($("form[role=form]"), function (data, ret) {
                    var treeNode = $('#tree').jstree(true).get_selected(true)[0];
                    $("#tree").jstree("deselect_all", true);
                    $('#tree').jstree('select_node', treeNode.id);
                });
            },
            ttl: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            rename: function () {
                Form.api.bindevent($("form[role=form]"));
            }

        };
        return Controller;
    });