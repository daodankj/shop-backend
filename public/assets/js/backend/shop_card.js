define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shop_card/index' + location.search,
                    add_url: 'shop_card/add',
                    edit_url: 'shop_card/edit',
                    del_url: 'shop_card/del',
                    multi_url: 'shop_card/multi',
                    import_url: 'shop_card/import',
                    table: 'shop_card',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'card_no', title: __('Card_no'), operate: 'LIKE'},
                        {field: 'card_password', title: __('Card_password'), operate: 'LIKE'},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'),searchList: {"0":'未激活',"1":'已激活'},formatter:Table.api.formatter.toggle},
                        {field: 'charge_time_text', title: __('充值时间'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});